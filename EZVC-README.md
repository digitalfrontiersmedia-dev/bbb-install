# EZVC BBB Install

Provides custom bbb-install.sh used with [EZVC Scalable AWS Cluster](https://bitbucket.org/digitalfrontiersmedia-dev/bbb-aws/src/main/) installer.

## bigbluebutton.properties override

- Requires reinstallation of bbb server
- Copy setting and it's description from `bigbluebutton.properties.original` to `bigbluebutton.properties.overrides`
- Commit and push to master
- Rerun bbb-install.sh on clean Ubuntu 16.04 server
- Overides file will be appended to the end of the stock properties file during installation.

## Test Installation

Real domain required. There must be a DNS entry pointing to bbb.example.net before installation

~~~
wget -qO- https://bitbucket.org/digitalfrontiersmedia-dev/bbb-install/raw/master/bbb-install.sh | bash -s -- -w -a -v xenial-22 -s bbb.example.net -e info@example.net
~~~

This command pulls down the latest version of `bbb-install.sh`, sends it to the Bash shell interpreter, and installs BigBlueButton using the parameters provided:

  * `-w` installs the uncomplicated firewall (UFW) to restrict access to TCP/IP ports 22, 80, and 443, and UDP ports in range 16384-32768,
  * `-a` installs the API demos (making it easy to do a few quick tests on the server), 
  * `-v xenial-22` installs the latest build of BigBlueButton 2.2.x, 
  * `-s` sets the server's hostname to be `bbb.example.com`, and
  * `-e` provides an email address for Let's Encrypt to generate a valid SSL certificate for the host.

